uzbek-wordlist (0.6-7) unstable; urgency=medium

  * QA upload.
  * Add debian/clean to remove the generated binary .bdic dictionary.
  * Add debian/upstream/metadata.
  * Remove obsolete debian/hunspell-uz.info-myspell.
  * Remove unneeded debian/hunspell-uz.dirs.
  * debian/control:
    - Bump Standards-Version to 4.6.2 (no changes).
    - Build-Depends on convert-bdic to enable building of the binary .bdic
      dictionary.
    - Add missing fields.
    - Cleanup layout.
  * debian/install:  Install the binary .bdic dictionary.  (Closes:  #1020465)
  * debian/lintian-overrides:  Add file to override a lintian wrong-section
    false positive.
  * debian/rules:  Build the binary .bdic dictionary.
  * debian/watch:  Update to point to the new GitHub repository.  Note that
    there are currently no tags and no releases on GitHub, but at least the
    files are there (the old location is offline).

 -- Soren Stoutner <soren@stoutner.com>  Thu, 18 Jan 2024 20:03:30 -0700

uzbek-wordlist (0.6-6) unstable; urgency=medium

  * QA upload.
  * Convert to 3.0 (quilt) format (Closes: #1007682).

 -- Bastian Germann <bage@debian.org>  Thu, 25 Aug 2022 14:21:30 +0200

uzbek-wordlist (0.6-5) unstable; urgency=medium

  * QA upload.
  * fix use debhelper compatibility level 13 (not 5);
    build-depend on debhelper-compat (not debhelper);
    closes: bug#965857, thanks to Niels Thykier
  * relax to use unversioned package relations:
    needed versions satisfied in all supported Debian releases
  * drop ancient package conflicts

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Feb 2022 11:50:33 +0100

uzbek-wordlist (0.6-4) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group. (see #841696)
  * Version thunderbird conflicts.
  * Remove link to dead upstream homepage.

 -- Adrian Bunk <bunk@debian.org>  Thu, 26 Jan 2017 00:46:53 +0200

uzbek-wordlist (0.6-3.2) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control:
    - Make sure we no longer install ancient openoffice.org
      snippets (Closes: #629072).
    - Fix lintian debhelper-but-no-misc-depends.
  * debian/rules:
    - No longer set ancient compatibility symlinks from
      /usr/share/myspell/dicts to /usr/share/hunspell contents.
    - Fix lintian debian-rules-missing-recommended-target build-{arch,indep}.

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 13 Sep 2011 15:29:44 +0200

uzbek-wordlist (0.6-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * move dict to /usr/share/hunspell with compat symlinks from
    /usr/share/myspell/dicts (closes: #541906)
  * move Homepage from Description to Homepage:

 -- Rene Engelhard <rene@debian.org>  Wed, 23 Sep 2009 23:45:49 +0200

uzbek-wordlist (0.6-3) unstable; urgency=low

  * Now it conflicts only against old ice*/mozillas found in sid
    (Closes: #427090).

 -- Mashrab Kuvatov <kmashrab@uni-bremen.de>  Fri, 22 Jun 2007 21:25:44 +0200

uzbek-wordlist (0.6-2) unstable; urgency=high

  * Added missing conflicts against mozilla-browser, iceape-browser, firefox,
    thunderbird, iceweasel, icedove (Closes: #405977).

 -- Mashrab Kuvatov <kmashrab@uni-bremen.de>  Fri, 12 Jan 2007 17:37:38 +0100

uzbek-wordlist (0.6-1) unstable; urgency=low

  * Initial release (Closes: #387824)

 -- Mashrab Kuvatov <kmashrab@uni-bremen.de>  Sun, 17 Sep 2006 00:11:32 +0200
